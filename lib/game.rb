require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game

  attr_accessor :player1, :player2, :board, :current_player

  def initialize(player1, player2)
    @player1 = player1
    @player2 = player2
    @player1.mark = :X
    @player2.mark = :O
    @board = Board.new
    @current_player = player1
  end

  def play_turn
    @board.place_mark(current_player.get_move, current_player.mark)
    switch_players!
  end

  def switch_players!
    if @current_player == player1
      @current_player = player2
    else
      @current_player = player1
    end
  end

  def play
    current_player.display(board)
    until board.over?
      play_turn
    end
    if game_winner
      game_winner.display(board)
      puts "#{game_winner.name} wins!"
    else
      puts "Tie!"
    end
  end

end

if $PROGRAM_NAME == __FILE__
  print "Please enter your name: "
  name = gets.chomp
  human = HumanPlayer.new(name)
  computer = ComputerPlayer.new("Bonzo")
  Game.new(human, computer).play
end
